const path = require('path');

const ReplacePlugin = require('webpack-plugin-replace');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

const CleanWebpackPlugin = require('clean-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');


module.exports = (env) => {
    const config = {
        mode: 'development',
        entry: ['babel-polyfill', './src/ts/app.ts'],
        resolve: {
            extensions: ['.tsx', '.ts', '.js']
        },
        output: {
            filename: 'app.[hash].js',
            publicPath: '',
            path: path.resolve(__dirname, 'dist')
        },
        optimization: {
            minimizer: [new TerserPlugin()]
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    enforce: 'pre',
                    loader: 'tslint-loader',
                    options: {/* Loader options go here */}
                },
                {
                    test: /\.ts?$/,
                    loader: 'babel-loader'
                },
                {
                    test: /\.js$/,
                    use: ['source-map-loader'],
                    enforce: 'pre'
                }]
        },
        plugins: [
            new CleanWebpackPlugin(['dist']),
            new WebpackNotifierPlugin({
                alwaysNotify: true,
                title: 'Game'
            }),
            new HtmlWebpackPlugin({template: 'src/index.html'}),
            new CopyWebpackPlugin([{from: 'src/assets', to: path.resolve(__dirname, 'dist') + '/assets'}]),
            new ReplacePlugin({
                values: {
                    '%webpack_version%': require('./package.json').version,
                    '%webpack_name%': require('./package.json').name,
                    '%webpack_environment%': env.NODE_ENV
                }
            })
        ]
    };

    if (env.NODE_ENV !== 'prod') {
        config.devtool = 'source-map';
    }

    return config;
};
