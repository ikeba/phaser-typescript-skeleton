module.exports = {
  verbose: false,
 // collectCoverage: true,
  preset: 'ts-jest',
  collectCoverageFrom: [
    'src/ts/modules/**/*.{js,jsx,ts,tsx}',
    '!src/**/*.spec.{js,jsx,ts,tsx}',
    '!src/ts/modules/helpers/**/*.*',
    'src/ts/app.ts'
  ],
  coverageDirectory: './coverage',
  coverageReporters: ['html', 'text'],
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
  testMatch: ["**/?(*.)+(spec).ts?(x)"],
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"]
};
