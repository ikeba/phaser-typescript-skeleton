To run locally:
1) npm install
2) npm run start
3) go to browser -> localhost:8080

To build
1) npm install
2) npm run build

To deploy
1) npm install
2) npm run deploy
