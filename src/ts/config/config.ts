import WebFontLoaderPlugin from '../helpers/webfontloader/webfontloader';

const phaserConfig = {
    type: Phaser.AUTO,
    parent: 'game',
    plugins: {
        global: [{
            key: 'WebFontLoader',
            plugin: WebFontLoaderPlugin,
            start: true
        }]
    },
    scale: {
        mode: Phaser.Scale.FIT,
        width: 1080 / 2,
        height: 1920 / 2
    },
    transparent: false
};

export const config = {
    phaserConfig
};
