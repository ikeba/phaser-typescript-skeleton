import {LoadingScene} from '../../game/scenes/loading/loading-scene';

class AssetLoader {
    public static scene;

    public static loadAssets(scene: LoadingScene, assets) {
        AssetLoader.scene = scene;

        if (assets.length === 0) {
            scene['onProgress'](100);
            scene['onLoadComplete']();
        }

        assets.map((el) => {
            if (el.type === 'image') {
                scene.load.image(el.name, el.url);
            }
            if (el.type === 'json') {
                scene.load.json(el.name, el.url);
            }
            if (el.type === 'spritesheet') {
                scene.load.spritesheet(el.name, el.url, {
                    frameHeight: el.frameHeight,
                    frameWidth: el.frameWidth
                });
            }
            if (el.type === 'atlas') {
                scene.load.atlas(el.name, el.textureUrl, el.atlasUrl);
            }
            if (el.type === 'html') {
                scene.load.html(el.name, el.url);
            }
            if (el.type === 'audio') {
                scene.load.audio(el.name, el.url);
            }
            if (el.type === 'audioSprite') {
                scene.load.audioSprite(el.name, el.jsonURL, el.audioURL);
            }
        });

        scene.load.on('progress', (data) => {
            const percent = Math.floor(data * 100);
            scene['onProgress'](percent);
        });

        scene.load.on('complete', () => scene['onLoadComplete']());
    }
}

export {
    AssetLoader
};
