import Phaser from 'phaser';
import {MainScene} from './game/scenes/main/main-scene';
import {config} from './config/config';

(async function() {

    if (window['appVersion']) {
        console.log(`%c [${window['appName']}] v${window['appVersion']} `, 'background: #e66700; color: white;');
    }

    const game = new Phaser.Game({
        ...config.phaserConfig,
        // scene: [LoadingScene, MainScene]
        scene: [MainScene]
    });

    game.scene.start(MainScene.is);

})();
