import {config} from '../../../config/config';

class MainScene extends Phaser.Scene {
    public static get is() {
        return 'MainScene';
    }

    public config: any;

    constructor() {
        super({key: MainScene.is});
    }

    public preload() {
        this.load['rexWebFont']({
            google: {
                families: ['Noto Sans Hebrew']
            }
        });
        this.load.on('webfontactive', (loader, font) => {
            console.log('The font was downloaded successfully', font);
        });
    }

    public init() {
        this.config = {
            width: config.phaserConfig.scale.width,
            height: config.phaserConfig.scale.height
        };
    }

    public create() {
        const text = new Phaser.GameObjects.Text(this, this.config.width / 2, 100, 'שלום עולם!', {
            fontSize: '75px',
            color: '#ffffff',
            fontFamily: 'Noto Sans Hebrew'
        });
        console.log('The text was created');
        text.setOrigin(0.5);
        this.add.existing(text);
    }
}

export {
    MainScene
};
