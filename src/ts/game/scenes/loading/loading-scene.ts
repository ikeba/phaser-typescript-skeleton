import {AssetLoader} from '../../../helpers/asset-loader/loader';
import {config} from '../../../config/config';

const GAME_WIDTH = config.phaserConfig.scale.width;
const GAME_HEIGHT = config.phaserConfig.scale.height;

const LOADING_RECT_BACKGROUND_COLOR = 0x808080;
const LOADING_RECT_COLOR = 0xe4e4e4;
const LOADING_RECT_TEXT_COLOR = 0xffffff;
const LOADING_RECT_HEIGHT = 30;

class LoadingScene extends Phaser.Scene {
    private percent: number;
    private loadingPercentText: Phaser.GameObjects.Text;
    private loadingRect: Phaser.GameObjects.Rectangle;

    public static get is() {
        return 'LoadingScene';
    }

    constructor() {
        super({
            key: LoadingScene.is,
        });
    }

    public onProgress(percent) {
        if (percent <= 100) {
            this.loadingRect.displayWidth = (GAME_WIDTH / 2) * (percent / 100);
            this.loadingPercentText.text = `${percent}%`;
        }
    }

    public onLoadComplete() {
        this.scene.stop(LoadingScene.is);
        this.scene.start('MainScene',
            {config});
    }

    public preload() {
        const LOADING_Y = GAME_HEIGHT * 0.5;

        this.percent = 0;

        this.loadingPercentText = new Phaser.GameObjects.Text(
            this,
            GAME_WIDTH / 2,
            LOADING_Y,
            '0%',
            {
                fontFamily: 'monospace',
                fontSize: '20px',
                color: `${LOADING_RECT_TEXT_COLOR}`,
                align: 'center'
            }
        );
        this.loadingPercentText.setOrigin(0.5, 0.5);
        const loadingRectBgr = this.add.rectangle(GAME_WIDTH / 4, LOADING_Y, GAME_WIDTH / 2, LOADING_RECT_HEIGHT, LOADING_RECT_BACKGROUND_COLOR);
        loadingRectBgr.setOrigin(0, 0.5);
        this.loadingRect = this.add.rectangle(GAME_WIDTH / 4, LOADING_Y, GAME_WIDTH / 2, LOADING_RECT_HEIGHT, LOADING_RECT_COLOR);
        this.loadingRect.setOrigin(0, 0.5);

        this.add.existing(loadingRectBgr);
        this.add.existing(this.loadingRect);
        this.add.existing(this.loadingPercentText);

        AssetLoader.loadAssets(this, []);
    }

    public update() {
        if (this.loadingPercentText.text !== `${this.percent}%`) {
            this.loadingPercentText.text = `${this.percent}%`;
        }
    }
}

export {
    LoadingScene
};
